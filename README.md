# Diane

![Diane screenshot](public/static/screenshot.png)


## Description

Diane est une assistante virtuelle par transcription orale utilisant l'API d'OpenAI

Diane is an oral transcription virtual assistant using the OpenAI API.


## API Key Configuration

Create a new file. Add the following line inside the file:

``` plaintext

OPENAI_API_KEY=sk-J6G...yourAPIkey
```
Save the file with the .env extension at the root of this project.


## Front demo

[link](https://renaudgweb.frama.io/diane/)


## Poof Of Concept

Create an 'uploads' directory at the same level as the Python script.

`mkdir uploads`

Add a 'question.wav' file to the directory.

Install openai and dotenv.

`pip install openai`

`pip install python-dotenv`

Run the Python script.

`python3 app.py`

Listen to the generated 'response.mp3' file.


``` python
import os
import sys
from dotenv import load_dotenv
from openai import OpenAI

load_dotenv()
client = OpenAI(api_key=os.environ.get('OPENAI_API_KEY'))

app = Flask(__name__)

UPLOAD_FOLDER = 'uploads'


audio_file= open(UPLOAD_FOLDER / "question.wav", "rb")
transcript = client.audio.transcriptions.create(
    model="whisper-1",
    file=audio_file
)

question = transcript.text
print(f"\n Question : ", {question}, "\n")


chat_response = client.chat.completions.create(
  model="gpt-3.5-turbo",
  messages=[
    {"role": "system", "content": "Je me nomme Diane, je suis une " \
             "assistante virtuelle par intelligence artificielle très " \
             "performante."},
    {"role": "user", "content": question}
  ]
)

response = chat_response.choices[0].message.content
print(f"Response : ", {response}, "\n")


text_response = chat_response.choices[0].message.content
audio_response = client.audio.speech.create(
    model="tts-1",
    voice="nova",
    input=text_response,
)

audio_response.stream_to_file(UPLOAD_FOLDER / "response.mp3")


if __name__ == '__main__':
    try:
        app.run(use_reloader=False, debug=True)
    except KeyboardInterrupt:
        print('\n -> Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
```


## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***


## LICENSE

MIT License

Copyright (c) 2023 RenaudG

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
