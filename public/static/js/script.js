const siriWave = new SiriWave({
    container: document.getElementById("siri-container"),
    width: 512,
    height: 128,
    style: "ios9"
});

let source = undefined;
let taskHandle = 0;
let spectrum, dBASpectrum;

// Pondération A (A-weighting) 
// https://www.softdb.com/difference-between-db-dba/
// https://en.wikipedia.org/wiki/A-weighting
const RA = f =>
    12194 ** 2 * f ** 4 /
    ((f ** 2 + 20.6 ** 2) * Math.sqrt((f ** 2 + 107.7 ** 2) * (f ** 2 + 737.9 ** 2)) * (f ** 2 + 12194 ** 2));

const A = f => 20 * Math.log10(RA(f)) + 2.0;

// Voir https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API/Visualizations_with_Web_Audio_API

// Fonction principale
function run() {
    // Obtention du flux audio du microphone de l'utilisateur
    const audioStream = navigator.mediaDevices.getUserMedia({ audio: true, video: false });
    // Fréquence de mise à jour de la visualisation
    const approxVisualisationUpdateFrequency = 5;

    // Traitement du flux audio
    audioStream
        .then(stream => Promise.all([stream, navigator.mediaDevices.enumerateDevices()]))
        .then(([stream, devices]) => {
            // Création du contexte audio
            let context = new (window.AudioContext || window.webkitAudioContext)();
            // Création de la source pour l'entrée audio
            source = context.createMediaStreamSource(stream);
            // Création du nœud d'analyse
            let analyser = context.createAnalyser();

            // Obtention des paramètres du flux audio
            const trackSettings = stream.getAudioTracks()[0].getSettings();
            const sampleRate = trackSettings.sampleRate || context.sampleRate;
            const deviceName = devices.find(device => device.deviceId === trackSettings.deviceId).label;

            console.log(`Sample rate: ${sampleRate} Hz,
Audio context sample rate: ${context.sampleRate} Hz,
Dynamic: ${trackSettings.sampleSize} bit
Device: ${deviceName}`);

            // Calcul du nombre total d'échantillons
            let totalNumberOfSamples = sampleRate / approxVisualisationUpdateFrequency;
            // Configuration de la taille de la FFT pour l'analyse
            analyser.fftSize = 2 ** Math.floor(Math.log2(totalNumberOfSamples));

            // Fonction de conversion d'unité pour les niveaux dB
            const uint8TodB = byteLevel =>
                (byteLevel / 255) * (analyser.maxDecibels - analyser.minDecibels) + analyser.minDecibels;

            console.log(`Frequency bins : ${analyser.frequencyBinCount}`);

            // Initialisation des pondérations pour chaque bande de fréquences
            const weightings = [-100];
            for (let i = 1; i < analyser.frequencyBinCount; i++) {
                weightings[i] = A(i * sampleRate / 2 / analyser.frequencyBinCount);
            }

            // Initialisation des tableaux pour les données de fréquence
            spectrum = new Uint8Array(analyser.frequencyBinCount);
            dBASpectrum = new Float32Array(analyser.frequencyBinCount);

            let waveForm = new Uint8Array(analyser.frequencyBinCount);

            // Connexion des éléments audio (source, analyser)
            source.connect(analyser);
            // Démarrage de l'animation de la forme d'onde
            siriWave.start();

            // Fonction de mise à jour de l'animation
            const updateAnimation = function (idleDeadline) {
                taskHandle = requestIdleCallback(updateAnimation, { timeout: 1000 / approxVisualisationUpdateFrequency });

                // Copie des données de fréquence de l'analyseur vers le tableau spectrum
                analyser.getByteFrequencyData(spectrum);

                // Calcul des niveaux de décibels pondérés A
                spectrum.forEach((byteLevel, idx) => {
                    dBASpectrum[idx] = uint8TodB(byteLevel) + weightings[idx];
                });

                // Calcul de diverses statistiques basées sur les données de fréquence
                const highestPerceptibleFrequency = dBASpectrum.reduce((acc, y, idx) => y > -90 ? idx : acc, 0);
                const totaldBAPower = dBASpectrum.reduce((acc, y) => acc + y);
                const meanFrequencyBin = dBASpectrum.reduce((acc, y, idx) => acc + y * idx) / totaldBAPower;
                const highestPowerBin = dBASpectrum.reduce(([maxPower, iMax], y, idx) =>
                    y > maxPower ? [y, idx] : [maxPower, iMax], [-120, 0]
                )[1];

                const highestDetectedFrequency = highestPerceptibleFrequency * (sampleRate / 2 / analyser.frequencyBinCount);
                const meanFrequency = meanFrequencyBin * (sampleRate / 2 / analyser.frequencyBinCount);
                const maxPowerFrequency = highestPowerBin * (sampleRate / 2 / analyser.frequencyBinCount);

                // Réglage de la vitesse de l'onde SiriWave en fonction de la fréquence maximale
                siriWave.setSpeed(maxPowerFrequency / 10e+3);

                // Recherche de l'amplitude maximale dans la forme d'onde
                analyser.getByteTimeDomainData(waveForm);
                const amplitude = waveForm.reduce((acc, y) => Math.max(acc, y), 128) - 128;

                // Mise à jour de l'amplitude de l'onde SiriWave
                siriWave.setAmplitude(amplitude / 128 * 10);
            };

            // Lancement de la première mise à jour de l'animation
            taskHandle = requestIdleCallback(updateAnimation, { timeout: 1000 / approxVisualisationUpdateFrequency });
        });
}
function stop() {
    cancelIdleCallback(taskHandle);
    siriWave.setAmplitude(0);
    siriWave.setSpeed(0);
    source.disconnect();
    siriWave.stop();
    source.mediaStream.getAudioTracks()[0].stop();
}
run();

let recordButton = document.getElementById('mic-button');
let radialBackground = document.getElementById('bg');
let recordImage = document.getElementById('img-svg');
let isRecording = false;

document.addEventListener('DOMContentLoaded', () => {
    recordButton.addEventListener('click', toggleRecording);
});

function toggleRecording() {
    if (!isRecording) {
        startRecording();
    } else {
        stopRecording();
    }

    isRecording = !isRecording;
}

function startRecording() {
    radialBackground.style.backgroundImage = 'radial-gradient(at 50% 50%, rgb(142, 3, 22) 0%, rgb(71, 7, 106) 80%)';
    recordImage.src = 'static/img/stop.svg';
}

function stopRecording() {
    radialBackground.style.backgroundImage = 'radial-gradient(at 50% 50%, rgb(94, 3, 80) 0%, rgb(28, 10, 130) 100%)';
    recordImage.src = 'static/img/mic.svg';
}
