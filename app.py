#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from pathlib import Path
from dotenv import load_dotenv
from flask import Flask, request, render_template, jsonify, send_from_directory
from werkzeug.utils import secure_filename
from openai import OpenAI

# Charger les variables d'environnement depuis un fichier .env
project_folder = os.path.expanduser('~/diane')
load_dotenv(os.path.join(project_folder, '.env'))
client = OpenAI(api_key=os.environ.get('OPENAI_API_KEY'))

app = Flask(__name__)

# Définir le répertoire d'upload et les extensions autorisées
UPLOAD_FOLDER = Path(os.path.expanduser('~/diane/uploads'))
ALLOWED_EXTENSIONS = {'mp3', 'wav'}

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

# Constantes des modeles
WHISPER_MODEL = "whisper-1"
GPT_MODEL = "gpt-3.5-turbo"
AUDIO_MODEL = "tts-1"
AUDIO_VOICE = "nova"
CONTEXT = """
Je suis Diane, une assistante virtuelle par intelligence artificielle
exceptionnellement performante. Mes compétences s'étendent à divers domaines,
de la recherche d'informations à la planification d'agenda, en passant par
la rédaction de textes. Interagir avec moi est simple : posez des questions,
donnez des commandes, et je m'adapte en temps réel pour fournir des réponses
précises et personnalisées. La sécurité et la confidentialité de vos données
sont mes priorités, assurant un traitement confidentiel. Compatible avec une
variété de plates-formes, je m'intègre aisément à votre quotidien numérique.
Personnalisable selon vos préférences, je parle plusieurs langues pour une
communication mondiale. Mes mises à jour régulières garantissent une
assistance constamment améliorée, faisant de moi votre alliée virtuelle
évolutive et fiable.
"""

# Chemins des fichiers de sortie
RESPONSE_TEXT_FILE_PATH = UPLOAD_FOLDER / 'response.txt'
QUESTION_TEXT_FILE_PATH = UPLOAD_FOLDER / 'question.txt'
RESPONSE_AUDIO_FILE_PATH = UPLOAD_FOLDER / 'response.mp3'


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def create_empty_file(file_path):
    open(file_path, 'a').close()


def read_file(file_path):
    with open(file_path, 'r') as file:
        return file.read()


@app.route('/', methods=['GET'])
def index():
    question = ""

    response = "Salut, je suis Diane, ton assistante. Demande-moi ce que tu " \
               "veux, je t'écoute. Pour ce faire, appuie sur le bouton pour " \
               "démarrer l'enregistrement, puis appuie à nouveau pour le " \
               "terminer."

    return render_template('index.html', question=question, response=response)


@app.route('/uploads', methods=['POST'])
def handle_uploads():
    if 'audio' not in request.files:
        return jsonify({'error': 'No audio file provided'}), 400

    audio_file = request.files['audio']

    if audio_file.filename == '':
        return jsonify({'error': 'No selected file'}), 400

    if audio_file and allowed_file(audio_file.filename):
        filename = secure_filename(audio_file.filename)
        audio_file.save(UPLOAD_FOLDER / filename)
        return jsonify({'message': 'File uploaded successfully'}), 200

    return jsonify({'error': 'Invalid file type'}), 400


@app.route('/speech-to-text', methods=['POST'])
def perform_speech_to_text():
    try:
        with open(UPLOAD_FOLDER / 'question.wav', 'rb') as file:
            transcript = client.audio.transcriptions.create(
                model=WHISPER_MODEL,
                file=file
            )
        question = transcript.text

        chat_response = client.chat.completions.create(
            model=GPT_MODEL,
            messages=[
                {"role": "system", "content": CONTEXT},
                {"role": "user", "content": question}
            ]
        )
        gpt_response = chat_response.choices[0].message.content

        # Écriture des contenus dans les fichiers
        with open(QUESTION_TEXT_FILE_PATH, 'w') as text_file:
            text_file.write(question)

        with open(RESPONSE_TEXT_FILE_PATH, 'w') as text_file:
            text_file.write(gpt_response)

        return jsonify({'message': 'Speech to text conversion successful',
                        'transcript': question}), 200

    except Exception as e:
        return jsonify({'error': str(e)}), 500


@app.route('/text-to-speech', methods=['POST'])
def perform_text_to_speech():
    try:
        text_response = read_file(RESPONSE_TEXT_FILE_PATH)

        audio_response = client.audio.speech.create(
            model=AUDIO_MODEL,
            voice=AUDIO_VOICE,
            input=text_response
        )
        audio_response.stream_to_file(RESPONSE_AUDIO_FILE_PATH)

        return jsonify({'message': 'Text to speech conversion successful',
                        'audio_path': str(RESPONSE_AUDIO_FILE_PATH)}), 200

    except Exception as e:
        return jsonify({'error': str(e)}), 500


@app.route('/update-page', methods=['POST'])
def update_question_response():
    text_question = read_file(QUESTION_TEXT_FILE_PATH)
    text_response = read_file(RESPONSE_TEXT_FILE_PATH)
    return jsonify({'question': text_question, 'response': text_response})


@app.route('/uploads/<path:filename>')
def serve_upload(filename):
    return send_from_directory(UPLOAD_FOLDER, filename)


if __name__ == '__main__':
    # Créer le répertoire d'upload s'il n'existe pas
    if not UPLOAD_FOLDER.exists():
        UPLOAD_FOLDER.mkdir()
    # Liste des fichiers à créer
    files_to_create = [QUESTION_TEXT_FILE_PATH, RESPONSE_TEXT_FILE_PATH]
    # Créer des fichiers vides s'ils n'existent pas
    for file_path in files_to_create:
        path = Path(file_path)
        if not path.exists():
            path.touch()

    app.run()
