const siriWave = new SiriWave({
    container: document.getElementById("siri-container"),
    width: 512,
    height: 128,
    style: "ios9"
});

let source = undefined;
let taskHandle = 0;
let spectrum, dBASpectrum, analyser, weightings, waveForm;
let approxVisualisationUpdateFrequency;
let sampleRate;

// Pondération A (A-weighting)
const RA = f =>
    12194 ** 2 * f ** 4 /
    ((f ** 2 + 20.6 ** 2) * Math.sqrt((f ** 2 + 107.7 ** 2) * (f ** 2 + 737.9 ** 2)) * (f ** 2 + 12194 ** 2));

const A = f => 20 * Math.log10(RA(f)) + 2.0;

// Fonction de conversion d'unité pour les niveaux dB
const uint8TodB = byteLevel =>
    (byteLevel / 255) * (analyser.maxDecibels - analyser.minDecibels) + analyser.minDecibels;

// Déclaration globale de la fonction updateAnimation
const updateAnimation = function (idleDeadline) {
    taskHandle = requestAnimationFrame(updateAnimation); // Utilisation de requestAnimationFrame

    // Copie des données de fréquence de l'analyseur vers le tableau spectrum
    analyser.getByteFrequencyData(spectrum);

    // Calcul des niveaux de décibels pondérés A
    spectrum.forEach((byteLevel, idx) => {
        dBASpectrum[idx] = uint8TodB(byteLevel) + weightings[idx];
    });

    // Calcul de diverses statistiques basées sur les données de fréquence
    const highestPerceptibleFrequency = dBASpectrum.reduce((acc, y, idx) => y > -90 ? idx : acc, 0);
    const totaldBAPower = dBASpectrum.reduce((acc, y) => acc + y);
    const meanFrequencyBin = dBASpectrum.reduce((acc, y, idx) => acc + y * idx) / totaldBAPower;
    const highestPowerBin = dBASpectrum.reduce(([maxPower, iMax], y, idx) =>
      y > maxPower ? [y, idx] : [maxPower, iMax], [-120, 0]
    )[1];

    const highestDetectedFrequency = highestPerceptibleFrequency * (sampleRate / 2 / analyser.frequencyBinCount);
    const meanFrequency = meanFrequencyBin * (sampleRate / 2 / analyser.frequencyBinCount);
    const maxPowerFrequency = highestPowerBin * (sampleRate / 2 / analyser.frequencyBinCount);

    // Réglage de la vitesse de l'onde SiriWave en fonction de la fréquence maximale
    siriWave.setSpeed(maxPowerFrequency / 10e+3);

    // Recherche de l'amplitude maximale dans la forme d'onde
    analyser.getByteTimeDomainData(waveForm);
    const amplitude = waveForm.reduce((acc, y) => Math.max(acc, y), 128) - 128;

    // Mise à jour de l'amplitude de l'onde SiriWave
    siriWave.setAmplitude(amplitude / 128 * 10);
};

// Fonction générique pour configurer l'analyseur
function configureAnalyzer(context, source, sampleRate) {
    analyser = context.createAnalyser();

    // Configuration de la taille de la FFT pour l'analyse
    analyser.fftSize = 1024; // Ajustez selon vos besoins

    // Initialisation des pondérations pour chaque bande de fréquences
    weightings = [-100];
    for (let i = 1; i < analyser.frequencyBinCount; i++) {
        weightings[i] = A(i * sampleRate / 2 / analyser.frequencyBinCount);
    }

    // Initialisation des tableaux pour les données de fréquence
    spectrum = new Uint8Array(analyser.frequencyBinCount);
    dBASpectrum = new Float32Array(analyser.frequencyBinCount);
    waveForm = new Uint8Array(analyser.fftSize);

    // Connexion des éléments audio (source, analyser)
    source.connect(analyser);
}

function runWaveWithMic() {
    navigator.mediaDevices.getUserMedia({ audio: true, video: false })
        .then(stream => Promise.all([stream, navigator.mediaDevices.enumerateDevices()]))
        .then(([stream, devices]) => {
            let context = audioContext;
            source = context.createMediaStreamSource(stream);

            const trackSettings = stream.getAudioTracks()[0].getSettings();
            sampleRate = trackSettings.sampleRate || context.sampleRate;
            const deviceName = devices.find(device => device.deviceId === trackSettings.deviceId).label;

            console.log(`Sample rate: ${sampleRate} Hz,
Audio context sample rate: ${context.sampleRate} Hz,
Dynamic: ${trackSettings.sampleSize} bit
Device: ${deviceName}`);

            // Calcul du nombre total d'échantillons
            let totalNumberOfSamples = sampleRate / approxVisualisationUpdateFrequency;
            approxVisualisationUpdateFrequency = totalNumberOfSamples / sampleRate;

            // Appel de la fonction générique pour configurer l'analyseur
            configureAnalyzer(context, source, sampleRate);

            // Démarrage de l'animation de la forme d'onde
            siriWave.start();

            // Lancement de la première mise à jour de l'animation
            taskHandle = requestAnimationFrame(updateAnimation);
        });
}

function runWaveWithMP3() {
    let context = audioContext;
    let source = context.createMediaElementSource(audioPlayer);

    // Appel de la fonction générique pour configurer l'analyseur
    configureAnalyzer(context, source, sampleRate);

    // Démarrage de l'animation de la forme d'onde
    siriWave.start();

    // Lancement de la première mise à jour de l'animation
    taskHandle = requestAnimationFrame(updateAnimation);
}

function stopWave() {
    cancelIdleCallback(taskHandle);
    siriWave.setAmplitude(0);
    siriWave.setSpeed(0);
    source.disconnect();
    siriWave.stop();
    if (source.mediaStream) {
        source.mediaStream.getAudioTracks()[0].stop();
    }
}

const bouncyCircle = new mojs.Shape({
    parent: '#bouncyCircle',
    shape: 'circle',
    fill: { '#ffffff': '#ffffff' },
    radius: { 30: 50 },
    duration: 1000,
    isYoyo: true,
    isShowStart: true,
    easing: 'elastic.inout',
    repeat: 1,
    onComplete: function () {
        this.replay();
    }
}).play();

let audioContext;
let mediaRecorder;
let audioChunks = [];
let audioPlayer = document.getElementById('audioPlayer');
let recordButton = document.getElementById('mic-button');
let radialBackground = document.getElementById('bg');
let recordImage = document.getElementById('img-svg');
let questionText = document.getElementById('question');
let responseText = document.getElementById('response');
let bouncyCircleDiv = document.getElementById('bouncyCircle');
let isRecording = false;

document.addEventListener('DOMContentLoaded', () => {
    // Lecture du message de bienvenue
    audioPlayer.play();
    // Initialiser l'enregistreur audio au chargement de la page
    navigator.mediaDevices.getUserMedia({ audio: true, video: false })
        .then((stream) => {
            audioContext = new (window.AudioContext || window.webkitAudioContext)();
            mediaRecorder = new MediaRecorder(stream);

            // Gérer les données disponibles pendant l'enregistrement
            mediaRecorder.ondataavailable = (event) => {
                if (event.data.size > 0) {
                    audioChunks.push(event.data);
                }
            };

            // Gérer l'arrêt de l'enregistrement
            mediaRecorder.onstop = () => {
                // Créer un Blob audio à partir des données enregistrées
                let audioBlob = new Blob(audioChunks, { type: 'audio/wav' });
                audioChunks = [];
                // Envoyer le fichier audio au serveur
                sendAudio(audioBlob);
            };
        })
        .catch((err) => {
            alert("Erreur lors de l'accès au microphone :", err);
        });

    runWaveWithMic();
    // Commencer l'enregistrement ou l'arrêter lors du clic sur le bouton
    recordButton.addEventListener('click', toggleRecording);
});

function toggleRecording() {
    if (!isRecording) {
        startRecording();
    } else {
        stopRecording();
    }

    isRecording = !isRecording;
}

function startRecording() {
    // Réinitialiser le tableau des morceaux audio
    audioChunks = [];
    mediaRecorder.start();
    radialBackground.style.backgroundImage = 'radial-gradient(at 50% 50%, rgb(142, 3, 22) 0%, rgb(71, 7, 106) 80%)';
    recordImage.src = 'static/img/stop.svg';
    questionText.textContent = 'Enregistrement...';
    responseText.textContent = '';
    console.log('Recording...');
}

function stopRecording() {
    mediaRecorder.stop();
    radialBackground.style.backgroundImage = 'radial-gradient(at 50% 50%, rgb(94, 3, 80) 0%, rgb(28, 10, 130) 100%)';
    recordImage.style.display = 'none';
    bouncyCircleDiv.style.zIndex = '1';
    bouncyCircle.play();
    console.log('Recording stopped');
}

function updatePage() {
    bouncyCircle.stop();
    bouncyCircleDiv.style.zIndex = '-9999';
    recordImage.src = 'static/img/mic.svg';
    recordImage.style.display = 'block';

    $.ajax({
        type: "POST",
        url: "/update-page",
        success: function (data) {
            $("#question").text(data.question);
            $("#response").text(data.response);
            console.log('Loaded');
        },
        error: function (error) {
            alert("Une erreur s'est produite lors de la mise à jour de la page :", error, "\nVeuillez réessayer plus tard.");
        }
    });
}

function sendAudio(audioBlob) {
    let reflections = {
        reflection: ['Permets-moi un moment de réflexion.', 'reflection.mp3'],
        reflection1: ['Donne-moi un instant pour y penser.', 'reflection_1.mp3'],
        reflection2: ['Accorde-moi le temps de bien réfléchir.', 'reflection_2.mp3'],
        reflection3: ['Je vais prendre un moment pour réfléchir à cela.', 'reflection_3.mp3'],
        reflection4: ['Laisse-moi prendre du recul et réfléchir.', 'reflection_4.mp3'],
        reflection5: ['Permets-moi de délibérer avant de donner une réponse.', 'reflection_5.mp3'],
        reflection6: ['Je vais m\'octroyer un moment de calme pour y réfléchir.', 'reflection_6.mp3'],
        reflection7: ['Je prends le temps de méditer sur cette question.', 'reflection_7.mp3']
    }
    // Sélection aléatoire d'une propriété de l'objet reflections
    let randomKey = Object.keys(reflections)[Math.floor(Math.random() * Object.keys(reflections).length)];
    // Utilisation des valeurs associées à la propriété sélectionnée
    let selectedReflection = reflections[randomKey];

    questionText.textContent = '💬';
    responseText.textContent = selectedReflection[0];
    audioPlayer.src = 'static/snds/'+selectedReflection[1];
    audioPlayer.play();
    console.log('Loading...');

    const formData = new FormData();
    formData.append('audio', audioBlob, 'question.wav');

    fetch('/uploads', {
        method: 'POST',
        body: formData
    })
    .then(response => response.json())
    .then(data => {
        // Envoyer la requête pour la conversion de la parole en texte
        fetch('/speech-to-text', {
            method: 'POST'
        })
        .then(data => {
            // Envoyer la requête pour la conversion du texte en parole
            fetch('/text-to-speech', {
                method: 'POST'
            })
            .then(response => response.json())
            .then(data => {
                // Éviter la mise en cache du fichier audio par valeur timestamp unique
                const audioUrl = '/uploads/response.mp3?cache=' + Date.now();
                audioPlayer.src = audioUrl;
                audioPlayer.play();
                //runWaveWithMP3();
                updatePage();
            })
            .catch(textToSpeechError => {
                alert("Erreur lors de la conversion texte-voix :", textToSpeechError);
            });
        })
        .catch(speechToTextError => {
            alert("Erreur lors de la conversion voix-texte :", speechToTextError);
        });
    })
    .catch(uploadError => {
        alert("Erreur lors de l'envoi du fichier audio :", uploadError);
    });
}
